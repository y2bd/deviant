import feedparser

_feedURL = 'http://backend.deviantart.com/rss.xml?q=gallery%3A'
_feeds = {}

def addFeed (user):
  """
  Adds a user to the feed pool.
  `user` should be the part before "deviantart.com" 
  in the url. This DOESN'T work with groups, so don't try.

  Returns a Boolean representing if the feed was valid
  and nonempty.

  Empty feeds (zero entries) will **not** be added.
  """

  if user in _feeds:
    return True

  parsed = feedparser.parse(_feedURL + user)

  if len(parsed.entries) > 0: 
    _feeds[user] = map(lambda x : _itemify(x, user), parsed.entries)
    return True

  return False

def getFeed (user, limit=-1, offset=0):
  """
  Returns a feed list for a particular user sorted chronologically descending.
  The list contains _FeedItem objects which are
  structured as followed:

  _FeedItem ->
    string title
    string artist
    string link
    time_struct date
    string desc # None if none present
    string thumb # url to thumbnail, None if none present
    string large # url to large display, None if none present
    string download # url to download, None if none present

  If limit is specified, it returns no more than `limit` items,
  else it returns all present items.

  If offset is specified, it returns starting at that offset of the list

  It returns None if the specified user isn't present.
  """

  if user not in _feeds:
    return None
  
  if limit < 0:
    return _feeds[user][offset:]

  return _feeds[user][offset:(offset + limit)]

def getAll (limit=-1, offset=0):
  """
  Returns a list containing items from all feeds sorted in
  chronological order.

  See getFeed() for details on the list's items.

  Limit and offset control how many items should be in the list,
  as well as where the list should begin.

  Returns None if there are no existing feeds.
  """

  if len(_feeds) <= 0:
    return None

  allItems = []

  for v in _feeds.values():
    allItems.extend(v)

  allItems = sorted(allItems, cmp=lambda a,b: cmp(a.date, b.date))[::-1]

  if limit < 0:
    return allItems[offset:]

  return allItems[offset:(offset + limit)]

def _itemify (entry, artist):
  summary, thumb, large, download = None, None, None, None
  
  # some posts don't have descriptions oddly enough
  if hasattr(entry, 'summary'):
    summary = entry.summary

  # if there's a thumbnail present, extract the largest one 
  # (the end of the list)
  if hasattr(entry, 'media_thumbnail'):
    thumb = entry.media_thumbnail[-1]['url']

  # are there media_content nodes?
  if hasattr(entry, 'media_content'):

    # there are two types:
    # image, and download
    # 
    # we don't know if they'll have both,
    # so we'll just cycle through all
    for mc in entry.media_content:
      if mc['medium'] == 'image':
        large = mc['url']
      if mc['medium'] == 'document':
        download = mc['url']

  return _FeedItem(entry.title, artist, entry.link, entry.published_parsed, summary, 
                   thumb, large, download)

class _FeedItem:
  
  def __init__ (self, title, artist, link, date, desc, thumb, large, download):
    self.title = title
    self.artist = artist
    self.link = link
    self.date = date
    self.desc = desc
    self.thumb = thumb
    self.large = large
    self.download = download

