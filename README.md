# Deviant

Deviant is a collection of python modules to interface with DeviantArt, a pretty much API-less website.

Dependencies:

* `beautifulsoup4`
* `feedparser`
* `requests`

They should all be installable using `pip install`.

## Example usage

```python
import account, friends, feeder

# initiate an Account object that we can pass around
me = account.Account(username='y2bd', password='hunter2')

# gives a list of _Friend objects
myFriends = friends.getFriends(me)

for friend in myFriends:
  print "%s (%s)" % (friend.username, friend.realname)

# let's initiate some feeds
# feeder.py will be reworked soon for better interoperability
# also it's really slow because internet :(
for friend in myFriends:
  feeder.addFeed(friend.username)

feeds = feeder.getAll(limit=25)

for feed in feeds:
  print "%s, by %s: $s" % (feed.title, feed.artist, feed.link)
```