import requests

class Account:

  def __init__(self, username, password):
    """
    Given a username and password, this generates an Account object
    that is used with `friends` and `messages`.

    If the username or password is invalid, this constructor raises
    a `ValueError`.
    """

    self.username = username
    self.session = requests.Session()

    payload = {
      'username': username,
      'password': password,
      'ref': 'http://www.deviantart.com',
      'reusetoken': '1'
    }

    post = self.session.post('https://www.deviantart.com/users/login', data=payload)

    if 'users/wrong-password' in post.url:
      raise ValueError('Either the username or password specified was incorrect.')

  def __repr__(self):
    return '<account.Account instance of \'%s\' at %s>' % (self.username, hex(id(self)))