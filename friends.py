import requests
import account

def getFriends(account, includeGroups=False):
  """
  Given an account.Account object, get all
  friends for that account. If the includeGroups parameter
  equals True, it will also include all groups
  that the account watches.

  It returns this as a list of _Friend objects,
  which are specified as below:

  _Friend ->
    string username # such as y2bd
    string realname # not necessarily their real name, just what's given. None if none specified
    string avatar # a link to their avatar, just because
    string page # a link to their dA page, in case you're too lazy to self-generate
    boolean isGroup # whether or not they're a group masquarading as a friend!

  Note: dA differentiates between 'friends' and 'people you just watch.' Just recall
  that 'Friend' checkbox when you watch someone. I'm really nowhere as picky, so 
  I just give everyone.
  """

  data = account.session.post('http://www.deviantart.com/global/difi.php?c[]=Friends;getFriendsMenu;0&t=json')

  # you can't say that they're not thorough
  friends = data.json()['DiFi']['response']['calls'][0]['response']['content']

  # remove groups if necessary
  if not includeGroups:
    friends = filter(lambda e: e['symbol'] != '#', friends)

  friends = map(_friendify, friends)

  return friends

def _friendify(entry):

  return _Friend(entry['username'], entry['realname'], entry['avatar'], entry['symbol'] == '#')

class _Friend:

  def __init__(self, username, realname, avatar, isGroup):
    self.username = username
    self.realname = realname if realname != "" else None
    self.avatar = avatar
    self.page = "http:%s.deviantart.com" % username
